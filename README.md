# Dockerize PHP Phalcon

Just a quick _wip_ to test __Phalcon__ with Docker…

The `Dockerfile` extends the official PHP image with _Phalcon_. The
`docker-compose.yml` sets up an _nginx_ container that uses it.

## Requirements

You need access to a Docker machine and the `docker-compose` command.

## Setup

You can override the default settings with _environment variables_ if you use
`docker-compose` (e.g. from an `.env` file) or set them as build arguments via
 the `docker build …` command:

- __NGINX_TAG__: `latest`
- __PHP_TAG__: `fpm`
- __PHALCON_VERSION__: `master` (use `v3.3.1` or similar for a specific version)

## Usage

Run: `docker-compose up`.

Browse to <http://localhost:8080/>.
